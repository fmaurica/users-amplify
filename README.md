Add User component following https://marmelab.com/react-admin/Tutorial.html

Set amplify following https://docs.amplify.aws/start/getting-started/setup/q/integration/react

```
% git checkout -b dev
Switched to a new branch 'dev'
```

```
% amplify init
Note: It is recommended to run this command from the root of your app directory
? Enter a name for the project usersamplify
The following configuration will be applied:

Project information
| Name: usersamplify
| Environment: dev
| Default editor: Visual Studio Code
| App type: javascript
| Javascript framework: react
| Source Directory Path: src
| Distribution Directory Path: build
| Build Command: npm run-script build
| Start Command: npm run-script start

? Initialize the project with the above configuration? Yes
Using default provider  awscloudformation
? Select the authentication method you want to use: AWS profile

For more information on AWS Profiles, see:
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html

? Please choose the profile you want to use personal-lou-amplify-profile
Adding backend environment dev to AWS Amplify Console app: d37iaolzhj7ihm
⠦ Initializing project in the cloud...

CREATE_IN_PROGRESS amplify-usersamplify-dev-201030 AWS::CloudFormation::Stack Thu Jul 15 2021 20:10:32 GMT+0200 (Central European Summer Time) User Initiated
CREATE_IN_PROGRESS AuthRole                        AWS::IAM::Role             Thu Jul 15 2021 20:10:36 GMT+0200 (Central European Summer Time)
CREATE_IN_PROGRESS UnauthRole                      AWS::IAM::Role             Thu Jul 15 2021 20:10:36 GMT+0200 (Central European Summer Time)
CREATE_IN_PROGRESS DeploymentBucket                AWS::S3::Bucket            Thu Jul 15 2021 20:10:36 GMT+0200 (Central European Summer Time)
CREATE_IN_PROGRESS AuthRole                        AWS::IAM::Role             Thu Jul 15 2021 20:10:36 GMT+0200 (Central European Summer Time) Resource creation Initiated
CREATE_IN_PROGRESS UnauthRole                      AWS::IAM::Role             Thu Jul 15 2021 20:10:37 GMT+0200 (Central European Summer Time) Resource creation Initiated
CREATE_IN_PROGRESS DeploymentBucket                AWS::S3::Bucket            Thu Jul 15 2021 20:10:37 GMT+0200 (Central European Summer Time) Resource creation Initiated
⠴ Initializing project in the cloud...

CREATE_COMPLETE AuthRole   AWS::IAM::Role Thu Jul 15 2021 20:10:55 GMT+0200 (Central European Summer Time)
CREATE_COMPLETE UnauthRole AWS::IAM::Role Thu Jul 15 2021 20:10:55 GMT+0200 (Central European Summer Time)
⠦ Initializing project in the cloud...

CREATE_COMPLETE DeploymentBucket                AWS::S3::Bucket            Thu Jul 15 2021 20:10:59 GMT+0200 (Central European Summer Time)
CREATE_COMPLETE amplify-usersamplify-dev-201030 AWS::CloudFormation::Stack Thu Jul 15 2021 20:11:00 GMT+0200 (Central European Summer Time)
✔ Successfully created initial AWS cloud resources for deployments.
✔ Initialized provider successfully.
Initialized your environment successfully.

Your project has been successfully initialized and connected to the cloud!

Some next steps:
"amplify status" will show you what you've added already and if it's locally configured or deployed
"amplify add <category>" will allow you to add features like user login or a backend API
"amplify push" will build all your local backend resources and provision it in the cloud
"amplify console" to open the Amplify Console and view your project status
"amplify publish" will build all your local backend and frontend resources (if you have hosting category added) and provision it in the cloud

Pro tip:
Try "amplify add api" to create a backend API and then "amplify publish" to deploy everything
```

```
% amplify push
✔ Successfully pulled backend environment dev from the cloud.

Current Environment: dev

| Category | Resource name | Operation | Provider plugin |
| -------- | ------------- | --------- | --------------- |

No changes detected
lou@PANCO-FMAURICA users-amplify % amplify publish

Please add hosting to your project before publishing your project
Command: amplify hosting add
```

## Hosting and environments

Following command is to be executed while configuring Amplilfy manually in AWS Console,
see `amplify-console-gitlab.png` and `amplify-console-environments.png`

```
% amplify hosting add
? Select the plugin module to execute Hosting with Amplify Console (Managed hosting with custom domains, Continuous deployment)
? Choose a type Continuous deployment (Git-based deployments)
? Continuous deployment is configured in the Amplify Console. Please hit enter once you connect your repository
No hosting URL found. Run 'amplify add hosting' again to set up hosting with Amplify Console.
lou@PANCO-FMAURICA users-amplify % amplify hosting add
? Select the plugin module to execute Hosting with Amplify Console (Managed hosting with custom domains, Continuous deployment)
? Choose a type Continuous deployment (Git-based deployments)
? Continuous deployment is configured in the Amplify Console. Please hit enter once you connect your repository
Amplify hosting urls:
┌──────────────┬───────────────────────────────────────────┐
│ FrontEnd Env │ Domain                                    │
├──────────────┼───────────────────────────────────────────┤
│ dev          │ https://dev.d37iaolzhj7ihm.amplifyapp.com │
└──────────────┴───────────────────────────────────────────┘
│ master       │ https://master.d37iaolzhj7ihm.amplifyapp.com │
└──────────────┴───────────────────────────────────────────┘
```
